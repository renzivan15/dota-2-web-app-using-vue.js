import axios from 'axios'
import config from '@/config/config'

export default {
  apiUserData (dotaId) {
    let url = `https://api.opendota.com/api/players/${dotaId}?api_key=${config.api_key}`
    return axios.get(url)
  },
  apiUserWL (dotaId) {
    let url = `https://api.opendota.com/api/players/${dotaId}/wl?api_key=${config.api_key}`
    return axios.get(url)
  },
  apiRecentMatches (dotaId) {
    let url = `https://api.opendota.com/api/players/${dotaId}/recentmatches?api_key=${config.api_key}`
    return axios.get(url)
  },
  apiMatches (dotaId) {
    let url = `https://api.opendota.com/api/players/${dotaId}/matches?api_key=${config.api_key}`
    return axios.get(url)
  },
  apiHeroes () {
    let url = `https://api.opendota.com/api/heroStats?api_key=${config.api_key}`
    return axios.get(url)
  },
  apiFriends (dotaId) {
    let url = `https://api.opendota.com/api/players/${dotaId}/peers?api_key=${config.api_key}`
    return axios.get(url)
  },
  apiUserHeroes (dotaId) {
    let url = `https://api.opendota.com/api/players/${dotaId}/heroes?api_key=${config.api_key}`
    return axios.get(url)
  }
}
