import Vue from 'vue'
import Vuex from 'vuex'

import api from '@/api/Api'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    userData: null,
    winloss: null,
    matches: null,
    recentMatches: null,
    heroes: null,
    friends: null,
    userHeroes: null,
    epochTime: null,
    limitMatchesVal: 20
  },
  getters: {
    getUserData: state => { return state.userData },
    getUserWL: state => { return state.winloss },
    getMatches: state => { return state.matches },
    getRecentMatches: state => { return state.recentMatches },
    getHeroes: state => { return state.heroes },
    getFriends: state => { return state.friends },
    getUserHeroes: state => { return state.userHeroes }
  },
  mutations: {
    mutateUserData (state, payload) {
      state.userData = payload.data
      // console.log('request userdata')
    },
    mutateUserWL (state, payload) {
      state.winloss = payload.data
      // console.log('request winloss')
    },
    mutateMatches (state, payload) {
      state.matches = payload.data
      // console.log('request matches')
    },
    mutateRecentMatches (state, payload) {
      state.recentMatches = payload.data
      // console.log('request recent matches')
    },
    mutateHeroes (state, payload) {
      state.heroes = payload.data
      // console.log('request heroes')
    },
    mutateFriends (state, payload) {
      state.friends = payload.data
      // console.log('request friends')
    },
    mutateUserHeroes (state, payload) {
      state.userHeroes = payload.data
      // console.log('request heroes played')
    },
    mutateConvertEpochTime (state, payload) {
      // let date = new Date(payload * 1000)
      // state.epochTime = date.toLocaleString()

      // console.log(state.limitMatchesVal)
    }
  },
  actions: {
    async actUserData ({ commit, dispatch }, dotaId) {
      try {
        let req = await api.apiUserData(dotaId)
        await commit('mutateUserData', req)
        await dispatch('actUserWL', dotaId)
        await dispatch('actMatches', dotaId)
        await dispatch('actRecentMatches', dotaId)
        await dispatch('actFriends', dotaId)
        await dispatch('actUserHeroes', dotaId)
      } catch (err) {
        console.log(err)
      }
    },
    async actUserWL ({ commit }, dotaId) {
      try {
        let req = await api.apiUserWL(dotaId)
        commit('mutateUserWL', req)
      } catch (err) {
        console.log(err)
      }
    },
    async actMatches ({ commit }, dotaId) {
      try {
        let req = await api.apiMatches(dotaId)
        commit('mutateMatches', req)
      } catch (err) {
        console.log(err)
      }
    },
    async actRecentMatches ({ commit }, dotaId) {
      try {
        let req = await api.apiRecentMatches(dotaId)
        commit('mutateRecentMatches', req)
      } catch (err) {
        console.log(err)
      }
    },
    async actHeroes ({ commit }) {
      try {
        let req = await api.apiHeroes()
        commit('mutateHeroes', req)
      } catch (err) {
        console.log(err)
      }
    },
    async actFriends ({ commit }, dotaId) {
      try {
        let req = await api.apiFriends(dotaId)
        commit('mutateFriends', req)
      } catch (err) {
        console.log(err)
      }
    },
    async actUserHeroes ({ commit }, dotaId) {
      try {
        let req = await api.apiUserHeroes(dotaId)
        commit('mutateUserHeroes', req)
      } catch (err) {
        console.log(err)
      }
    }
  }
})
export default store
