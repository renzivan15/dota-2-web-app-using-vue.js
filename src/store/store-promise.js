import Vue from 'vue'
import Vuex from 'vuex'

import api from '@/api/Api'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    userData: null,
    winloss: null,
    matches: null,
    recentMatches: null,
    heroes: null,
    friends: null,
    userHeroes: null
  },
  getters: {
    getUserData: state => { return state.userData },
    getUserWL: state => { return state.winloss },
    getMatches: state => { return state.matches },
    getRecentMatches: state => { return state.recentMatches },
    getHeroes: state => { return state.heroes },
    getFriends: state => { return state.friends },
    getUserHeroes: state => { return state.userHeroes }
  },
  mutations: {
    mutateUserData (state, payload) {
      state.userData = payload.data
      console.log('request userdata')
    },
    mutateUserWL (state, payload) {
      state.winloss = payload.data
      console.log('request winloss')
    },
    mutateMatches (state, payload) {
      state.matches = payload.data
      console.log('request matches')
    },
    mutateRecentMatches (state, payload) {
      state.recentMatches = payload.data
      console.log('request recent matches')
    },
    mutateHeroes (state, payload) {
      state.heroes = payload.data
      console.log('request heroes')
    },
    mutateFriends (state, payload) {
      state.friends = payload.data
      console.log('request friends')
    },
    mutateUserHeroes (state, payload) {
      state.userHeroes = payload.data
      console.log('request heroes played')
    }
  },
  actions: {
    async actUserData ({ commit, dispatch }, dotaId) {
      let xhr = api.apiUserData(dotaId)
      xhr.then((res) => {
        commit('mutateUserData', res)
        dispatch('actUserWL', dotaId)
        dispatch('actMatches', dotaId)
        dispatch('actRecentMatches', dotaId)
        dispatch('actFriends', dotaId)
        dispatch('actUserHeroes', dotaId)
      }).catch((err) => {
        console.log(err)
      })
    },
    async actUserWL ({ commit }, dotaId) {
      let xhr = api.getUserWL(dotaId)
      xhr.then((res) => {
        commit('getUserWL', res)
      }).catch((err) => {
        console.log(err)
      })
    },
    actMatches ({ commit }, dotaId) {
      let xhr = api.apiMatches(dotaId)
      xhr.then((res) => {
        commit('mutateMatches', res)
      }).catch((err) => {
        console.log(err)
      })
    },
    actRecentMatches ({ commit }, dotaId) {
      let xhr = api.apiRecentMatches(dotaId)
      xhr.then((res) => {
        commit('mutateRecentMatches', res)
      }).catch((err) => {
        console.log(err)
      })
    },
    actHeroes ({ commit }) {
      let xhr = api.apiHeroes()
      xhr.then((res) => {
        commit('mutateHeroes', res)
      }).catch((err) => {
        console.log(err)
      })
    },
    actFriends ({ commit }, dotaId) {
      let xhr = api.apiFriends(dotaId)
      xhr.then((res) => {
        commit('mutateFriends', res)
      }).catch((err) => {
        console.log(err)
      })
    },
    actUserHeroes ({ commit }, dotaId) {
      let xhr = api.apiUserHeroes(dotaId)
      xhr.then((res) => {
        commit('mutateUserHeroes', res)
      }).catch((err) => {
        console.log(err)
      })
    }
  }
})
export default store
