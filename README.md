# Dota 2 Profile Web App Using Vue.js

### References
- [Bootstrap-Vue Documentation](https://bootstrap-vue.js.org/docs)
- [OpenDota API Documentation](https://docs.opendota.com)
- [Game Modes](https://github.com/odota/dotaconstants/blob/master/json/game_mode.json)

### Getting Started
```bash
# Clone Repository
git clone https://renzivan@bitbucket.org/renzivan/dota-2-web-app-using-vue.js.git

# Install all dependencies
npm install

# Run project
npm run serve
```

### Firebase Hosting
```bash
# Create project in firebase console and initialize. Set public directory to dist
firebase init

# Build the Vue application. It will build the project to the dist directory
npm run build

# Preview before deploying to firebase
firebase serve

# Deploy project
firebase deploy
```

### Issues
```bash
# Error: Module build failed (from ./node_modules/sass-loader/lib/loader.js):
npm rebuild node-sass
```